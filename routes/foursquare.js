var express = require('express');
var config = require('config-node')({dir: 'config'});
var foursquare = (require('foursquarevenues'))(config.client_id, config.client_secret);
var _ = require('underscore');
var async = require('async');
var log4js = require('log4js');
var logger = log4js.getLogger();
var pg = require('pg');
var request = require('request');
var connString = 'postgres://root:rootroot@wetrip.csi7ramz45nz.ap-southeast-1.rds.amazonaws.com/wetripdevelopment';
// var client = new pg.Client(conString);
const token = config.token || 'AVBSER51DCJ5OORVGBY3VNDDCZGF3RPGSJJURSFWUETXTEAM';
var params = {
	'll': config.ll
};
var elasticsearch = require('elasticsearch');
var elasticClient = new elasticsearch.Client({
	host: process.env.ELASTICSEARCH || 'http://52.77.184.71:9200/',
	log: 'trace'
});

var router = express.Router();

function handleRes (res, body, callback) {
	if (res.statusCode >= 300) {
		return callback(body, null);
	} else {
		return callback(null, JSON.parse(body));
	}
}

function getVenue (params, callback) {
	var urlString = 'https://api.foursquare.com/v2/venues/' + params.venue_id + '?' + 'oauth_token=' + token + '&' + 'v=20160528';
	return request(urlString, function (error, response, body) {
		return handleRes(response, body, callback);
	});
}

function transformCategories (categories) {
	var categoryStr = '{';
	_.each(categories, function (category) {
		categoryStr = categoryStr + category.name + ',';
	});
	categoryStr = categoryStr.substring(0, categoryStr.length - 1);
	categoryStr = categoryStr + '}';
	return categoryStr;
}

router.get('/venue/explore', (req, res) => {
	params.query = req.query.term;
	foursquare.exploreVenues(params, function (error, venues) {
		if (!error) {
			logger.info(JSON.stringify(venues));
		}
		return res.status(200).json(venues);
	});

});

router.get('/venue/search', (req, res) => {
	params.query = req.query.term;
	foursquare.getVenues(params, function (error, venues) {
		if (!error) {
			logger.info(JSON.stringify(venues));
		}
		return res.status(200).json(venues);
	});

});

router.get('/venue/:id', (req, res) => {
	params.venue_id = req.params.id;
	var save = req.query.save || 'false';

	getVenue(params, function (error, response) {
		if (error) {
			logger.error(error);
			return res.status(404).json({ok: false,message: error});
		}

		var venue = response.response.venue;
		var categories = null;
		var categoryStr = null;
		if (venue.categories.length > 0) {
			var categories = venue.categories;
			categoryStr = transformCategories(categories);
		}

		if (save === 'true') {
			logger.info('Save to database');
			pg.connect(connString, function (err, client, done) {
				if (err) {
					logger.error('could not connect to postgres', err);
					return;
				}
				async.waterfall([
					function (callback) {
						// INSERT INTO places (id, name, location_id, rating, latitude, longitude, vicinity, description, region_id, default_budget, created_at, updated_at) VALUES (1, 'Công viên nước Đầm Sen', 'ChIJlw0hwZgudTERz8OwnvH2O6w', 4.3, 10.7689894, 106.6359362, '3 Hòa Bình, Phường 3', 'Quam esse aut exercitationem commodi. Quae dolore velit aut quas repudiandae. Animi nostrum quas rerum earum at blanditiis. Quia odit nihil tempore fugiat modi quis et adipisci. Sapiente saepe accusantium et impedit aut rerum maiores. Qui voluptatibus repellat eveniet aut. Aspernatur mollitia omnis maxime rerum quis in voluptatem qui. Quia neque praesentium libero perspiciatis maxime fugiat est aliquid. Optio velit doloremque pariatur necessitatibus ut omnis. Neque deleniti possimus dicta enim ea reiciendis. Laudantium et culpa et minus. Ut quis velit doloremque aliquid vel voluptatem. Qui magni recusandae voluptas sit. Aut voluptatem autem aut in et ut at. Debitis pariatur deleniti aut consequatur doloribus hic vitae ex. Ratione et omnis laboriosam qui dolor. Quia porro laborum consectetur est. Eum possimus dolore consequatur occaecati. Quidem sed illo totam temporibus. Consequatur iste rerum voluptas illum totam omnis non.', 1, 555112, '2016-04-25 01:29:03.211833', '2016-04-25 01:29:03.211833');
						client.query(
							'INSERT INTO places (name, location_id, rating, latitude, longitude, vicinity, description, region_id, default_budget, created_at, updated_at, sub_categories)' +
							'values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)',
							[venue.name, venue.id, venue.rating, venue.location.lat, venue.location.lng, venue.location.address, null, 1, null, new Date(), new Date(), categoryStr],
							function (err, result) {
								if (err) {
									logger.error('error running query', err);
									return;
								}
								callback();
							});
					},
					function (callback) {
						client.query(
							'select * from places where location_id = $1',
							[venue.id],
							function (err, result) {
								if (err) {
									logger.error('error running query', err);
									return;
								}
								logger.info('Selected id : ' + result.rows[0].id);
								callback(null, result);
							});
					},
					function (result, callback) {
						async.parallel([
							function (callback) {
								logger.info('Prepare to index elasticsearch');
								elasticClient.update({
									index: 'locations',
									type: 'vn',
									id: result.rows[0].id,
									body: {
										doc: result.rows[0],
										upsert: result.rows[0]
									}
								}, function (error, response) {
									if (error) {
										logger.error(error);
										return;
									}
									logger.info('Indexed successfully in Elasticsearch : ' + JSON.stringify(response));
								// return consumer.commitOffset({topic: topic, partition: partition, offset: m.offset, metadata: 'optional'});
								});
							},
							function (callback) {
								logger.info('Inserting photos');
								logger.info('Id of place to add photos : ' + result.rows[0].id);
								if (venue.photos.groups[0]) {
									var photos = venue.photos.groups[0].items;
									logger.info(photos);
									async.each(photos, function (item, callback) {
										link = item.prefix + item.width + 'x' + item.height + item.suffix;
										client.query(
											'INSERT INTO place_photos (place_id, photo_reference, height, width, created_at, updated_at, photo_link)' +
											'values ($1,$2,$3,$4,$5,$6,$7)',
											[result.rows[0].id, null, item.height, item.width, new Date(), new Date(), link],
											function (err, result) {
												if (err) {
													logger.error('error running query', err);
													return;
												}
												logger.info('Insert photo successfully');

											});
									}, function (err) {
										if ( err ) {
											logger.error(err);
											return;
										}
										done();
									});
								}
							}
						]);

					}
				]);

			});
		}
		return res.status(200).json(venue);
	});

});

module.exports = router;
