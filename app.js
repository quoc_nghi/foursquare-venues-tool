var express = require('express');
var config = require('config-node')({dir: 'config'});
var schedule = require('node-schedule');
var log4js = require('log4js');
var logger = log4js.getLogger();
var bodyParser = require('body-parser');
var Server = require('http').Server;
var foursquare = require('./routes/foursquare');
var async = require('async');
var pg = require('pg');
var fs = require('fs');
var _ = require('underscore');
var randomService = require('./lib/random');
var venue = require('./lib/venue');

var app = express();
var http = Server(app);

app.use(bodyParser.json());

// Default routes message
app.get('/', function (req, res) {
	return res.status(200).json({message: 'Welcome to We Trip API'});
});

app.use('/foursquare', foursquare);

http.listen(3002, () => {
	logger.setLevel(process.env.LOG_LEVEL || 'DEBUG');

	logger.info('Server run on 3002');
});

function shuffleArray (array) {
	for (var i = array.length - 1; i > 0; i--) {
		var j = Math.floor(Math.random() * (i + 1));
		var temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	return array;
}

var job1 = schedule.scheduleJob('0 0 */1 * * *', function () {
	var array = fs.readFileSync('location.txt').toString().split('\n');
	array = shuffleArray(array);
	// var random = randomService.randomLL(1, 3);
	var params = {
		'll': '10.8230989,106.6296638',// HCM
		'llAcc': process.env.LLACC || '100000.0',
		'limit': process.env.LIMIT || 50,
		'client_id': 'YRNDDQNYU0JM1PQJVBVBB3CRTRMD0P3XOZA4SGHTX2JKC50H',
		'client_secret': 'P5N3MCPUNBZMYZNPTIY2IC4DJ030P2CVJU50BP2QATWRPWTF'
	};
	venue.getVenuesAnInsert(array, params, 1, function (err, callback) {});
	logger.info('HCM location');
});

var job2 = schedule.scheduleJob('0 20 */1 * * *', function () {
	var array = fs.readFileSync('location.txt').toString().split('\n');
	array = shuffleArray(array);
	// var random = randomService.randomLL(1, 3);
	var params = {
		'll': '10.8230989,106.6296638',// HCM
		'llAcc': process.env.LLACC || '100000.0',
		'limit': process.env.LIMIT || 50,
		'client_id': 'YRNDDQNYU0JM1PQJVBVBB3CRTRMD0P3XOZA4SGHTX2JKC50H',
		'client_secret': 'P5N3MCPUNBZMYZNPTIY2IC4DJ030P2CVJU50BP2QATWRPWTF'
	};
	venue.getVenuesAnInsert(array, params, 1, function (err, callback) {});
	logger.info('HCM location');
});

var job3 = schedule.scheduleJob('0 40 */1 * * *', function () {
	var array = fs.readFileSync('location.txt').toString().split('\n');
	array = shuffleArray(array);
	// var random = randomService.randomLL(1, 3);
	var params = {
		'll': '10.8230989,106.6296638',// HCM
		'llAcc': process.env.LLACC || '100000.0',
		'limit': process.env.LIMIT || 50,
		'client_id': 'YRNDDQNYU0JM1PQJVBVBB3CRTRMD0P3XOZA4SGHTX2JKC50H',
		'client_secret': 'P5N3MCPUNBZMYZNPTIY2IC4DJ030P2CVJU50BP2QATWRPWTF'
	};
	venue.getVenuesAnInsert(array, params, 1, function (err, callback) {});
	logger.info('HCM location');
});
//
// var job2 = schedule.scheduleJob('0 20 */1 * * *', function () {
// 	var array = fs.readFileSync('location.txt').toString().split('\n');
// 	array = shuffleArray(array);
// 	// var random = randomService.randomLL(1, 3);
// 	var params = {
// 		'll': '21.0031177,105.8201408',// Ha Noi
// 		'llAcc': process.env.LLACC || '100000.0',
// 		'limit': process.env.LIMIT || 50,
// 		'client_id': 'ZL32FZSA0LN1S5ZGXUYV5EC0GJDVX35RNWRKOMGKFT0VW5NH',
// 		'client_secret': 'FUDWOGPRZ5YBS4OF3ZBYB3JSA1YE2E31J1PZPRRQAFL0IU0J'
// 	};
// 	venue.getVenuesAnInsert(array, params, 2, function (err, callback) {});
// 	logger.info('Ha Noi location!');
// });
//
// var job3 = schedule.scheduleJob('0 40 */1 * * *', function () {
// 	var array = fs.readFileSync('location.txt').toString().split('\n');
// 	array = shuffleArray(array);
// 	// var random = randomService.randomLL(1, 3);
// 	var params = {
// 		'll': '16.0470788,108.2062293',// Da nang
// 		'llAcc': process.env.LLACC || '100000.0',
// 		'limit': process.env.LIMIT || 50,
// 		'client_id': 'ZA3P12GQVBZY3OKHGGQW52UICUVM5XIDJAY4OLN2DLDEELG4',
// 		'client_secret': 'LYNMYXQDTRPF2SZT554EBAD1PTUAOJSPQFNJDNPTYL0NRUM5'
// 	};
// 	venue.getVenuesAnInsert(array, params, 3, function (err, callback) {});
// 	logger.info('Da Nang location!');
// });
