var request = require('request');
var log4js = require('log4js');
var logger = log4js.getLogger();
var config = require('config-node')({dir: 'config'});
const token = config.token;

function handleRes (res, body, callback) {
	if (res.statusCode >= 300) {
		return callback(body, null);
	} else {
		return callback(null, JSON.parse(body));
	}
}

function getVenue (params, callback) {
	logger.info('Token : ' + params.token);
	var urlString = 'https://api.foursquare.com/v2/venues/' + params.venue_id + '?' + 'oauth_token=' + params.token + '&' + 'v=20160528';
	return request(urlString, function (error, response, body) {
		return handleRes(response, body, callback);
	});
}

module.exports = {
	getVenue: getVenue
};
