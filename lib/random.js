var _ = require('underscore');

function randomLL (min, max) {
	var random = _.random(min, max);
	switch (random) {
		// Ho Chi Minh
		case 1:
			return [1, '10.8230989,106.6296638'];
		// Ha Noi
		case 2:
			return [2, '21.0031177,105.8201408'];
		// Da Nang
		case 3:
			return [3, '16.0470788,108.2062293'];
		// An Giang
		case 4:
			return [4, '10.5215836,105.1258955'];
		// Ba Ria - Vung Tau
		case 5:
			return [5, '10.5417397,107.2429976'];
		// Bac Giang
		case 6:
			return [6, '21.3014947,106.6291304'];
		// Bac Can
		case 7:
			return [7, '22.3032923,105.876004'];
		// Bac Lieu
		case 8:
			return [8, '9.2515555,105.5136472'];
		// Bac Ninh
		case 9:
			return [9, '21.121444,106.1110501'];
		// Ben Tre
		case 10:
			return [10, '10.1081553,106.4405872'];
		// Binh Duong
		case 11:
			return [11, '11.3254024,106.477017'];
		// Binh Phuoc
		case 12:
			return [12, '11.7511894,106.7234639'];
		// Binh Thuan
		case 13:
			return [13, '10.9804603,108.2614775'];
		// Binh Dinh
		case 14:
			return [14, '13.7829673,109.2196634'];
		// Cà Mau
		case 15:
			return [15, '8.9624099,105.1258955'];
		// Cần Thơ
		case 16:
			return [16, '10.0451618,105.7468535'];
		// Cao Bang
		case 17:
			return [17, '22.635689,106.2522143'];
		// Gia Lai
		case 18:
			return [17, '13.8078943,108.109375'];
		default:
			return [1, '10.8230989,106.6296638'];
	}
}

function randomCredentials () {
	var random = _.random(1, 3);
	switch (random) {
		default:
			return ['ZL32FZSA0LN1S5ZGXUYV5EC0GJDVX35RNWRKOMGKFT0VW5NH', 'FUDWOGPRZ5YBS4OF3ZBYB3JSA1YE2E31J1PZPRRQAFL0IU0J'];
		case 2:
			return ['YRNDDQNYU0JM1PQJVBVBB3CRTRMD0P3XOZA4SGHTX2JKC50H', 'P5N3MCPUNBZMYZNPTIY2IC4DJ030P2CVJU50BP2QATWRPWTF'];
		case 3:
			return ['ZA3P12GQVBZY3OKHGGQW52UICUVM5XIDJAY4OLN2DLDEELG4', 'LYNMYXQDTRPF2SZT554EBAD1PTUAOJSPQFNJDNPTYL0NRUM5'];
	}
}

// function staticCredentials () {
// 	var credentials = [];
// 	var cre1 = {
// 		client_id : 'ZL32FZSA0LN1S5ZGXUYV5EC0GJDVX35RNWRKOMGKFT0VW5NH',
// 		client_secret : 'FUDWOGPRZ5YBS4OF3ZBYB3JSA1YE2E31J1PZPRRQAFL0IU0J',
// 	}
// }

module.exports = {
	randomLL: randomLL,
	randomCredentials: randomCredentials
};
